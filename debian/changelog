php-pinba (1.1.2-18) unstable; urgency=medium

  * Enable PHP 8.3 packages

 -- Ondřej Surý <ondrej@debian.org>  Mon, 15 Jul 2024 09:52:09 +0200

php-pinba (1.1.2-17) unstable; urgency=medium

  * Bump default version to PHP 8.4

 -- Ondřej Surý <ondrej@debian.org>  Sat, 06 Jul 2024 17:33:56 +0200

php-pinba (1.1.2-16) unstable; urgency=medium

  * Express that PHP 7.0 - 8.2 versions are supported

 -- Ondřej Surý <ondrej@debian.org>  Sat, 10 Dec 2022 17:46:22 +0100

php-pinba (1.1.2-15) unstable; urgency=medium

  * Use new X-PHP header to specify supported PHP versions and default
    version

 -- Ondřej Surý <ondrej@debian.org>  Sat, 10 Dec 2022 17:32:39 +0100

php-pinba (1.1.2-14) unstable; urgency=medium

  * Workaround the empty-binary-package lintian problem
  * Remove the default version override

 -- Ondřej Surý <ondrej@debian.org>  Sat, 10 Dec 2022 17:28:35 +0100

php-pinba (1.1.2-13) unstable; urgency=medium

  * Regenerate d/control for PHP 8.2

 -- Ondřej Surý <ondrej@debian.org>  Fri, 09 Dec 2022 13:58:48 +0100

php-pinba (1.1.2-12) unstable; urgency=medium

  * Fix the d/control for PHP 8.1

 -- Ondřej Surý <ondrej@debian.org>  Mon, 03 Jan 2022 15:01:30 +0100

php-pinba (1.1.2-11) unstable; urgency=medium

  * Regenerate d/control for PHP 8.1

 -- Ondřej Surý <ondrej@debian.org>  Sat, 01 Jan 2022 13:09:54 +0100

php-pinba (1.1.2-9) unstable; urgency=medium

  * Recompile for PHP 7.4 until the transition is complete

 -- Ondřej Surý <ondrej@debian.org>  Fri, 26 Nov 2021 11:25:58 +0100

php-pinba (1.1.2-8) unstable; urgency=medium

  * Update the packaging to dh-php >= 4~

 -- Ondřej Surý <ondrej@debian.org>  Wed, 10 Nov 2021 12:01:02 +0100

php-pinba (1.1.2-7) unstable; urgency=medium

  * Bump B-D to dh-php >= 3.1~

 -- Ondřej Surý <ondrej@debian.org>  Sat, 20 Feb 2021 18:07:41 +0100

php-pinba (1.1.2-6) unstable; urgency=medium

  * Override the PHP_DEFAULT_VERSION

 -- Ondřej Surý <ondrej@debian.org>  Sat, 20 Feb 2021 17:08:04 +0100

php-pinba (1.1.2-5) unstable; urgency=medium

  * Revert arch:all change, as it breaks shlibs:Depends

 -- Ondřej Surý <ondrej@debian.org>  Sat, 20 Feb 2021 13:21:42 +0100

php-pinba (1.1.2-4) unstable; urgency=medium

  * The main dummy package is arch:all
  * Bump dh-php Build-Depends to >= 3.0~

 -- Ondřej Surý <ondrej@debian.org>  Sat, 20 Feb 2021 07:51:12 +0100

php-pinba (1.1.2-3) unstable; urgency=medium

  * Sync the changelog with Debian bullseye
  * Update d/gbp.conf for debian/main branch
  * Update standards version to 4.5.1 (no change)

 -- Ondřej Surý <ondrej@debian.org>  Tue, 16 Feb 2021 12:10:58 +0100

php-pinba (1.1.2-2) unstable; urgency=medium

  * Lower the dh-php dependency to Debian sid version
  * Update d/gbp.conf for Debian bullseye

 -- Ondřej Surý <ondrej@debian.org>  Sun, 14 Feb 2021 16:40:42 +0100

php-pinba (1.1.2-1) unstable; urgency=medium

  * Finish conversion to debhelper compat level 10
  * New upstream version 1.1.2
  * Build the extension only for PHP 7
  * Update for dh-php >= 2.0 support

 -- Ondřej Surý <ondrej@debian.org>  Sat, 17 Oct 2020 06:39:42 +0200

php-pinba (1.1.1-3) unstable; urgency=medium

  * No change rebuild for Debian buster

 -- Ondřej Surý <ondrej@debian.org>  Wed, 28 Aug 2019 09:21:02 +0200

php-pinba (1.1.1-2) unstable; urgency=medium

  * Update the minimal pinba version to 7.0.0

 -- Ondřej Surý <ondrej@sury.org>  Thu, 08 Aug 2019 11:01:25 +0200

php-pinba (1.1.1-1) unstable; urgency=medium

  * Add Pre-Depends on php-common >= 0.69~
  * New upstream version 1.1.1
  * Rebase patches for php-pinba_1.1.1

 -- Ondřej Surý <ondrej@sury.org>  Thu, 08 Aug 2019 08:55:06 +0200

php-pinba (1.1.0-5) unstable; urgency=medium

  * Team upload.
  * Bump the required dh-php version to >= 0.33~
  * Fix the Vcs-* links
  * Fix the Maintainer: address
  * Run wrap-and-sort -a over debian/
  * Add patch for PHP 7.3 support (Courtesy of Remi Collet)

 -- Ondřej Surý <ondrej@debian.org>  Tue, 16 Oct 2018 13:07:22 +0000

php-pinba (1.1.0-4) unstable; urgency=medium

  * d/rules: compile for all supported PHP versions.
  * d/control: bump Standards-Version.

 -- Vincent Bernat <bernat@debian.org>  Sat, 02 Dec 2017 17:44:08 +0100

php-pinba (1.1.0-3) unstable; urgency=medium

  * d/rules: prevent network access during tests (to qa.php.net).
  * d/rules: provide versioned package matching PHP version.

 -- Vincent Bernat <bernat@debian.org>  Sat, 17 Sep 2016 07:36:04 +0200

php-pinba (1.1.0-2) unstable; urgency=medium

  * Use dynamic libprotobuf-c from system.

 -- Vincent Bernat <bernat@debian.org>  Sat, 17 Sep 2016 07:27:46 +0200

php-pinba (1.1.0-1) unstable; urgency=medium

  * New upstream release.
  * Do the PHP7 transition. Closes: #821553, #821704.
  * d/copyright: mention the embedded protobuf files.
  * d/watch: update to use GitHub releases.
  * d/patches: add a patch to update pinba.proto.

 -- Vincent Bernat <bernat@debian.org>  Fri, 16 Sep 2016 20:26:57 +0200

php-pinba (1.0.0-2) unstable; urgency=low

  * Maintain the package inside the PHP PEAR team
  * Move packaging to pkg-php-tools
  * debian/prerm: disable pinba, not geoip (Closes: #715437)
  * Update copyright to format 1.0
  * Use canonical URI in Vcs-* fields

 -- Prach Pongpanich <prachpub@gmail.com>  Tue, 09 Jul 2013 16:53:57 +0700

php-pinba (1.0.0-1) unstable; urgency=low

  * Initial packaging. Closes: #705400.

 -- Vincent Bernat <bernat@debian.org>  Sat, 25 May 2013 15:22:52 +0200
